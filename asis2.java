/**
 * @(#)asis2.java
 *
 *
 * @author 
 * @version 1.00 2018/4/5
 */
 import java.io.*;
class Persona  {  
  	private String nombre;  
   	private int a_nac;
  	  
    public void  asignar  (String n, int a)  
   	{ 
   		nombre = n; a_nac = a;
   	}   	
   		   
   	public String  get_nom()
   	{ 
   		return nombre;
   	}
   	  
   	public int  get_a()
   	{ 
   		return a_nac;
   	}
   	
   	public int calcular_a�o_nac(int a�o_ac)
   	{
   		return  a�o_ac   -    a_nac;
   	}    
 }  	  
class Alumno extends Persona 
 {  
    private int totcredito, puntos;     
    private  String carrera; 
         
    public void  asignar (String n, int a, int t, int p, String carr)   
    { 
      asignar (n,a);
      totcredito = t; 
      puntos = p; 
      carrera = carr;     
     }
        
    public double calcular_indice ()  
    { double indice;
      indice =	puntos/totcredito;  
      return indice ;   } 
      
    public int get_totcre(){
    	return totcredito;  }
    
    public int get_puntos() {
    	return puntos; }
    	
    public String get_carrera(){
    	return carrera; } 
   }

public class asis2 
{
    public static void main(String arg[]) 
    {
    	InputStreamReader isr = new InputStreamReader(System.in);
    	BufferedReader br = new BufferedReader(isr);
    	boolean op= true;
    	int opc,a=0,t,p,e=1,ac=2018;	//t: totcredit  a:a�o nac, p:puntos
    	String n="",carre="";
    	char sex;
    	Alumno al;
    	//Empleado em;
    		while(op)
    		{
    			System.out.println("\n\t\t\t\tMen�");
    			System.out.print("1- Alumno \n\n2- Empleado \n\n3- Salir");
    			System.out.print("\n\nElija una opci�n: ");
    		  try
    			{
    			opc=Integer.parseInt(br.readLine());
				 switch(opc)
    				{case 1: System.out.println("\n\t1- Estudiante \n\t2- Empleado");
    						System.out.print("�Eres un estudiante o un empleado?:");

    						e=Integer.parseInt(br.readLine());
    						System.out.print("\t Ingrese su nombre: ");
    						n=br.readLine();
    						System.out.print("\n\t Ingrese su a�o de nacimiento: ");
    						a=Integer.parseInt(br.readLine());
    						if(e==1)
    						{System.out.print("\n\t Ingrese su carrera: ");
    						carre= br.readLine();	}
    						else
    						{ if(e==2)
    							{System.out.println("\n\tSexo Masculino o Femenino? m/f");
    							sex= (br.readLine()).charAt(0);
    							}
    							else
    								System.out.print("Recuerda que solo debe hay 2 opciones");
    						}
    						break;
    				case 2: al= new Alumno();
    						System.out.print("\n\n\t\t\t\tBienvenido");
    						switch(e)
    						{
    							case 1: System.out.print("\n\tA continuaci�n procederemos a calcular tu �ndice y tu edad "+
    										"\n\tPara ello deber�s ingresar tus puntos y el total de creditos");
    									System.out.println("\n\t\t\t\tPrecione ENTRER para continuar");
    									br.readLine();
    									System.out.print("\n\t Ingrese sus puntos: ");
    									p=Integer.parseInt(br.readLine());
    									System.out.print("\n\t Ingrese el total de creditos: ");
    									t=Integer.parseInt(br.readLine());
    									al.asignar(n,a,t,p,carre);
    									System.out.println("======================================================================");
    									System.out.println("\n\tHola "+al.get_nom()+" aqu� estan los resultados:");
    									System.out.println("\tTienes "+al.calcular_a�o_nac(ac)+" a�os");
    									System.out.println("\tTienes "+al.get_puntos()+" puntos, un total de "+al.get_totcre()+" creditos");
    									System.out.println("\tTu �ndice es de: "+al.calcular_indice());
    									System.out.println("\tY estas estudiando: "+al.get_carrera());
    									System.out.print("\n\n\t\t\tPULSE ENTER PARA CONTINUAR");
    									br.readLine();
    						}
    				break;
    				case 3: System.out.print("\n\n\t\t\t Hasta luego ^_^");
    						op=false;
    						break;
    				default: System.out.print("\tLa opci�n seleccionada no existe, Recuerda que solo son 3 opciones");
    				}
    			   }
    				catch(IOException ioe)
    				{
    					System.out.println("\n\t No dejes espacios en blancos");
    				}
    				catch(NumberFormatException nfe)
    				{
    					System.out.println("\n\tRecuerda ingresar n�meros enteros");
    				}
    				catch(Exception ex)
    				{
    					System.out.println("\tWowow veamos que pas�");
    					System.err.println("\n\n"+ex);
    				}
    			
    		}
    }
    
    
}