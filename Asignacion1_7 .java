/**
 * @(#)Asignacion1_7.java
 *
 *
 * @author
 * @version 1.00 2018/4/16
 */
import java.io.*;

abstract class Cuentas_Bancarias
{
	protected String Nombre;
	protected float Saldo_Actual;
	Cuentas_Bancarias()
    {Saldo_Actual=0;
	}
	Cuentas_Bancarias(String Nombre)
	{ this.Nombre=Nombre;
	}
	public void DepositarCuenta(float money)
	{	Saldo_Actual+=money;	}
	
	public float Enviar_Saldo_Actual()
	{	return Saldo_Actual;	}
	
	abstract  void RetirarCuenta(float money);
	abstract  Double Calcular_Interes();
}

class Cuenta_Regular extends Cuentas_Bancarias
{
	private static final Double interes=0.06;//6%
	Cuenta_Regular()
    {super();
	}
	Cuenta_Regular(String Nombre)
    {super(Nombre);
	}
	void RetirarCuenta(float money)
	{
		Saldo_Actual=Saldo_Actual-money-1;
	}
	Double Calcular_Interes()
	{
		return Saldo_Actual*interes+Saldo_Actual;
	}
}

class Cuenta_Dorada extends Cuentas_Bancarias
{
	private static final Double interes=0.05;
    Cuenta_Dorada()
    {super();
    }
    Cuenta_Dorada(String Nombre)
    {super(Nombre);
	}
	void RetirarCuenta(float money)
	{
		Saldo_Actual=Saldo_Actual-money;
	}
	Double Calcular_Interes()
	{
		return Saldo_Actual*interes;
	}
}

class Asignacion1_7
{
    public static void main(String[] args)
   	{

   	  InputStreamReader isr=new InputStreamReader(System.in);
   	  BufferedReader br=new BufferedReader(isr);
   	  Cuentas_Bancarias cuba=null;		//Super
   	  Cuenta_Regular cr= new Cuenta_Regular();//subb
   	  Cuenta_Dorada cd= new Cuenta_Dorada();//sub
   	  boolean opc=true,repit;
   	  String usuario="",cuen="";
   	  char cuenta=' ';
   	  float deposito=0,retirar=0;
   	  int opcion;
		

   	  while(opc)

   	  {
   	  	try
   	  	{
   	  	System.out.println("\n\t\t\t\t Menu ");
   	    System.out.println("\n\t 1. Creaci�n de cuenta " );  //Opcion esta de mas, corregir.
   	    System.out.println("\t 2. Depositar ");
   	    System.out.println("\t 3. Retirar ");
   	    System.out.println("\t 4. Estado actual de la cuenta ");
   	    System.out.println("\t 5. Salir ");
   	    System.out.print("\n Por favor, seleccione una opci�n: ");
   	    opcion=Integer.parseInt(br.readLine());

   	    switch(opcion)
   	    {
   	     case 1:
   	     	do{
   	     		try{
   	     	  	repit=false;
   	     	  	System.out.println("\n\n***********************************************************************************************");
   	     		System.out.print("\t\t\t �Qu� tipo de cuenta desea? \n\tIntroduzca D una para cuenta Dorada o R para una regular: ");
   	     		cuenta=br.readLine().charAt(0);
   	     		if((cuenta!='D' & cuenta!='d')&&(cuenta!='r' & cuenta!='R')) //Solo aceptamos D,d o R,r
   	     			{System.out.print("\n\tPor favor siga las indicaciones.\n\tSolo puedes ingresar D o R");
   	     			 repit=true;
   	     			}
   	     		else
   	     		{	System.out.print("\n\tIntroduzca su Nombre: ");
   	     			usuario=br.readLine();
   	     			if(cuenta=='D'||cuenta=='d')
   	     			{
   	     		 	cuen="Cuenta Dorada";
   	     		 	cd= new Cuenta_Dorada(usuario);
   	     		 	cuba= cd;
   	     			}
   	     		    else
   	     		    {
   	     		    	cuen="Cuenta Regular";
   	     		    	cr= new Cuenta_Regular(usuario);
   	     		    	cuba= cr;	
   	     		    }
   	     			System.out.print("\n Bienvenido "+cuba.Nombre+" a PolyBank \n\tDonde no te fallamos.");
   	     		}
   	     		}
   	     		catch(IOException ioe)
   	  			{
   	  	 			System.err.println("\n\tError E/S");
   	  	 			repit=true;
   	  			}
   	  			catch(NumberFormatException nfe)
   	  			{
	   	  		System.err.println("\n\tSolo ingresa un n�mero entero");
	   	  		repit=true;
	   	  		}
   	        }while(repit);
   	     break;
   	     case 2:
   	     	do{ repit=false;
   	     		try{
   	     			System.out.print("\n\tMonto a depositar: ");
   	     	 		deposito=Float.valueOf(br.readLine()).floatValue();
   	     	 		if(deposito>0)
   	     	 			cuba.DepositarCuenta(deposito);
   	     	 		else
   	     	 		{	System.out.print("\n\tEl saldo que desea ingresar es menor a un balboa. \n\tPor favor vuelva a intentarlo.");
   	     	 			repit=true;
   	     	 		}
   	     		   }
   	     		catch(IOException ioe)
   	  			{
   	  	 			System.err.println("\n\tError E/S");
   	  	 			repit=true;
   	  			}
   	  			catch(NumberFormatException nfe)
   	  			{
	   	  		System.err.println("\n\tSolo ingresa un n�mero entero");
	   	  		repit=true;
	   	  		}
   	     	}while(repit);
   	     break;
   	     case 3:
   	     	do{ repit=false;
   	     		try{
	   	     		 System.out.println("\n\t�Cuanto desea retirar?");
   			     	 retirar=Float.valueOf(br.readLine()).floatValue();
   			     	 if(retirar>0)	//Solo acepta de un balboa en adelante.
   			     	 {if(cuenta=='r' | cuenta =='R')
   	     	 			{
   	     	 			if(retirar>cuba.Enviar_Saldo_Actual())
   	     	 			System.out.print("\n\tLo sentimos, pero el monto a retirar ha superado el saldo actual.");
   	     	 			else
   	     	 			{
   	     	 			if(retirar==cuba.Enviar_Saldo_Actual())
   	     	 				{System.out.println("\n\tDisculpe pero por politica del banco se le descuenta un balboa por cada retiro que efectu�"+
   	     	 					"\n\tya que su cuenta es regular y el monto que desea retirar es igual al saldo acumulado en su cuenta.");
   	     	 				}
   	     	 				else
   	     	 					cuba.RetirarCuenta(retirar);
   	     	 			}
   	     	 			}
   	     	 			else
   	     	 				cuba.RetirarCuenta(retirar);
   			     	 }
   			     	 else
   			     	 {	System.out.println("\n\tLa cantidad que desea retirar es invalida, por favor vuelva a intentarlo.");
   			     	 	repit=true;
   			     	 }
   	     		}
   	     		catch(IOException ioe)
   	  			{
   	  	 		System.err.println("\n\tError E/S");
   	  	 		repit=true;
   	  			}
   	  			catch(NumberFormatException nfe)
   	  			{
	   	  		System.err.println("\n\tSolo ingresa un n�mero entero");
	   	  		repit=true;
	   	  		}
   	        }while(repit);
   	        break;
   	     case 4:
			 System.out.println("\n--------PolyBank�--------");
   	     	 System.out.println("\tCliente: "+cuba.Nombre);
   	     	 System.out.println("\tTipo de Cuenta: "+cuen);
   	     	 System.out.println("\tSaldo actual: "+cuba.Enviar_Saldo_Actual());
   	     	 System.out.print("\tInteres anual: "+cuba.Calcular_Interes());
   	     	
   	     break;
   	     case 5: System.out.print("\n-------------PolyBank�-----------");
   	     		 System.out.print("\n\tGracias por preferirnos. \n\tHasta luego.");
   	     		 opc=false;
   	     break;  
   	     	default: System.out.println("\n\tEsta opci�n no esta disponible.\n\tVuelva a intentarlo.");
   	    }
   	  	}
   	  	catch(IOException ioe)
   	  	{
   	  	 System.err.println("\n\tError E/S");
   	  	}
   	  	catch(NumberFormatException nfe)
   	  	{
   	  		System.err.println("\n\tSolo ingresa un n�mero entero");
   	  	}
   	  	catch(NullPointerException npe)
   	  	{
   	  		System.err.println("\n\tEl objeto est� vacio por favor ingrese a la opci�n 1");
   	  	}
   	  	catch(Exception e)
   	  	{
   	  		System.err.println("\n\tHa ocurrido un error de tipo: "+e+"\n\tPor favor notificarlo");
   	  	}
   	  	finally
   	  	{System.out.println("\n\n***********************************************************************************************");
   	  	}
   	  }
    }
}