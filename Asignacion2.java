/**
 * @(#)Asignacion2.java
 *
 *
 * @author 
 * @version 1.00 2018/4/5
 */

import java.io.*;

//Superclase
class Persona  
{  
  	private String nombre;  
   	private int a_nac;
  	  
    public void  asignar  (String n, int a)  
   	{ 
   		nombre = n; 
   		a_nac = a;
   	}   	
   		   
   	public String  get_nom()
   	{ 
   		return nombre;
   	}
   	  
   	public int  get_a()
   	{ 
   		return a_nac;
   	}
   	
   	public int calcular_a�o_nac(int a�o_ac)
   	{
   		return  a�o_ac   -    a_nac;
   	}    
 } 
 
 //SUBCLASE ALUMNOS	 	  
 class Alumno extends Persona 
 {  
 	
    private int totcredito, puntos;     
    private  String carrera; 
         
    public void  asignar (String n, int a, int t, int p, String carr)   
    { 
      asignar (n,a);
      totcredito = t; 
      puntos = p; 
      carrera = carr;     
    }  
        
    public double calcular_indice ()  
    { 
      double indice; 
      indice =  puntos/totcredito;   
      return indice;   
    } 
      
    public int get_totcre()
    {
    	return totcredito;  
    }
    
    public int get_puntos() 
    {
    	return puntos; 
    }
    	
    public String get_carrera()
    {
    	return carrera; 
    }
    
 }
 
 //SUBCLASE EMPLEADOS
 class Empleado extends Persona
 {
 	private int gen, a�oin, horastrab;
 	private double tarif;
 		
 	public void asignar(String n, int a, int g, int aingr, int h, double t)
 	{
 		asignar(n, a);
 		gen=g;
 		a�oin=aingr;
 		horastrab=h;
 		tarif=t;
 	}
 	
 	public int get_a�odeingreso()
 	{
 		return a�oin;
 	}
 	
 	public int calcular_a�os_trab()
   	{
   		int a�o_ac=2018;
   		return  a�o_ac   -    a�oin;
   	}
   	
   	public void bono()
   	{
   		int ac=2018;
   		if(calcular_a�o_nac(ac)>=60 && gen==1)
   		{
   			System.out.println("Usted ha recibido un bono de $50.00, debido a que es");
   			System.out.println("perteneciente a la Tercera Edad.");	 
   			System.out.println("-----------------------------------------------------");
   		}
   		if(calcular_a�o_nac(ac)>=55 && gen==2)
   		{
   			System.out.println("Usted ha recibido un bono de $50.00, debido a que es");
   			System.out.println("perteneciente a la Tercera Edad.");
   			System.out.println("-----------------------------------------------------");
   		}
   	}
   	
   	public double salario_horas()
   	{
   		double salario;
   		salario=horastrab*tarif*20;
   		return  salario;
   	}
   	
   	public void a�os_empresa()
   	{
   		double calc;
   		if(calcular_a�os_trab()>=20)
   		{
   			calc=salario_horas()*0.20;
   			System.out.println("Bonificaci�n Navide�a: "+calc);
   		}
   		else
   		{
   			System.out.println("Bonificaci�n Navide�a: $35.00");
   		}
   	}
 	
 }   
   	
//MAIN
class Asignacion2 {

    public static void main(String [] args) {
    	
    	InputStreamReader isr = new InputStreamReader(System.in);
    	BufferedReader br = new BufferedReader(isr);
    	Alumno alum;
    	Empleado empl;
    	String nombre, nomcarrera, gener="", op;
    	int creditosto, creditosob, nac, ed, horastr, a�oingr, gen, opc=0, a�oac=2018;
    	double tarifa;
    	
    	while(opc!=3)
    	{
    		try
    		{
    			System.out.println("\n\n\n=====================================================");
    			System.out.println("\t\t\t\t       MEN�       ");
    			System.out.println("=====================================================");
    			System.out.println("1.Alumno \n2.Empleado \n3.Salir");
    			System.out.println("-----------------------------------------------------");
    			System.out.print("Introduzca una opci�n: ");
    			opc=Integer.parseInt(br.readLine());
    			System.out.println("-----------------------------------------------------");
    			op="r";
    			switch(opc)
    			{
    				case 1: while(op=="r")
    						{
    						alum= new Alumno();
    						System.out.println("\n\n=====================================================");
    					    System.out.println("\t\t\t        BIENVENIDO");
    					    System.out.println("=====================================================\n");
    						System.out.println("A continuaci�n le pediremos algunos datos del estudiante.");
    						System.out.println("\nIntroduzca su nombre: ");
    						nombre=br.readLine();
    						System.out.println("\nIntroduzca el a�o en que naci�: ");
    						nac=Integer.parseInt(br.readLine());
    						System.out.println("\nIntroduzca el nombre de su carrera: ");
    						nomcarrera=br.readLine();
    						System.out.println("\nIntroduzca el total de cr�ditos: ");
    						creditosto=Integer.parseInt(br.readLine());
    						System.out.println("\nIntroduzca la cantidad de cr�ditos obtenidos: ");
    						creditosob=Integer.parseInt(br.readLine());
    						alum.asignar(nombre, nac, creditosob, creditosto, nomcarrera);
    						
    						System.out.println("\n\n=====================================================");
    						System.out.println("\t\t\t    DATOS DEL ALUMNO");
    						System.out.println("=====================================================");
    						System.out.println("\nNombre del Estudiante: "+alum.get_nom());
    						System.out.println("Edad actual: "+alum.calcular_a�o_nac(a�oac));
    						System.out.println("Carrera: "+alum.get_carrera());
    						System.out.println("Cr�ditos totales: "+alum.get_puntos());
    						System.out.println("Cr�ditos obtenidos: "+alum.get_totcre());
    						System.out.println("Su �ndice es: "+alum.calcular_indice());
    						System.out.println("-----------------------------------------------------");
    						System.out.println("\n\n\t\t\t\tPresione Enter para volver al men�.");
    						op=br.readLine();
    						}	
    						break;
    							
    				case 2: while(op=="r")
    						{
    						empl= new Empleado();
    						System.out.println("\n\n=====================================================");
    						System.out.println("\t\t\t        BIENVENIDO");
    						System.out.println("=====================================================\n");
    						System.out.println("A continuaci�n le pediremos algunos datos del empleado.");
    						System.out.println("\nIntroduzca su nombre: ");
    						nombre=br.readLine();
    						System.out.println("\nIntroduzca su g�nero.\nIngrese 1 para Masculino o 2 para Femenino:");
    						gen=Integer.parseInt(br.readLine());
    						System.out.println("\nIntroduzca el a�o en que naci�: ");
    						nac=Integer.parseInt(br.readLine());
    						if(nac>2000)
    						{
    							System.out.println("\n===============================================================================");
    							System.out.println("\t�Vaya! Eres menor de edad. Ingresa a la opci�n para ALUMNOS en el Men�.");
    							System.out.println("===============================================================================");
    							System.out.println("\n\n\t\t\t\tPresione Enter para volver al men�.");
    							op=br.readLine();
    							break;
    						}
    						System.out.println("\nIntroduzca el a�o en el que ingres� a la empresa: ");
    						a�oingr=Integer.parseInt(br.readLine());
    						System.out.println("\nIntroduzca las horas trabajadas: ");
    						horastr=Integer.parseInt(br.readLine());
    						System.out.println("\nIntroduzca la tarifa por hora: ");
    						tarifa=Double.parseDouble(br.readLine());
    						empl.asignar(nombre, nac, gen, a�oingr, horastr, tarifa);
    						if(gen==1)
    						{
    							gener="Masculino";
    						}
    						else 
    						{
    							gener="Femenino";
    						}
    						System.out.println("\n\n=====================================================");
    						System.out.println("\t\t\t    DATOS DEL EMPLEADO");
    						System.out.println("=====================================================");
    						System.out.println("\nNombre del Empleado: "+empl.get_nom());
    						System.out.println("Edad actual: "+empl.calcular_a�o_nac(a�oac));
    						System.out.println("Sexo: "+gener);
    						System.out.println("A�o de ingreso: "+empl.get_a�odeingreso());
    						System.out.println("A�os laborando: "+empl.calcular_a�os_trab());
    						System.out.println("Tarifa por hora: $"+tarifa);
    						System.out.println("Horas de trabajo: "+horastr);
    						System.out.println("Salario: $"+empl.salario_horas());
    						System.out.println("-----------------------------------------------------");
    						empl.a�os_empresa();
    						System.out.println("-----------------------------------------------------");
    						empl.bono();
    						System.out.println("\n\n\t\t\t\tPresione Enter para volver al men�.");
    						op=br.readLine();
    						}
    						break;
    						
    				case 3: System.out.println("\n\n\n\t\t\tGracias por utilizar nuestro programa. �Hasta luego!.\n\n\n");
    						break;
    			}
    		}
    		
    		catch(IOException i)
    		{
    			System.out.println("\n\nS�lo puedes ingresar letras. Int�ntalo de nuevo.\n\n");
    		}
    		catch(NumberFormatException n)
    		{
    			System.out.println("\n\nS�lo puedes ingresar n�meros. Int�ntalo de nuevo.\n\n");
    		}
    	}
    	
    	
    		
    }
    
    
}