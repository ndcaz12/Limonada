/**
 * @(#)Limonada.java
 *
 *
 * @author 
 * @version 1.00 2018/4/9
 */

import java.io.*;
class SinAzucar // Es la superclase
{ private int vasos;
  SinAzucar() //Es el constructor
  {	vasos = 100;	}
  
  public int mostrar_vasos()
  {  return vasos;    }
  										
  public double calcularPrecio(int ventavasos) 
  {	vasos = vasos - ventavasos;
	return 0.45 * ventavasos;
  }
  
  public int total_vasos_utilizados()
  {
  	return 100-vasos;
  }
}
class ConAzucar extends SinAzucar
{
	private int azucar;
	private double subt;
	ConAzucar()
	{	
		azucar = 80;
	}
	public void carga(int cucharadas,int a)
	{
		subt=calcularPrecio(a);
		azucar=azucar-cucharadas*a;
	}
	public int azu_rest()
	{ 
		return azucar;	
	}
	public double C_Azucar()
	{
		subt=subt+total_vasos_utilizados()*0.05;
		return subt;
	}
	public int total_azucar()
	{ return 80-azucar;	}
}
class Limonada
{

    public static void main(String arg[]) throws IOException
    {
    	InputStreamReader isr = new InputStreamReader(System.in);
    	BufferedReader br = new BufferedReader (isr);
    	boolean opc=true,reply=false;
    	int azucar,op,cuchara,vasos;
    	char anotherone;
    	double total=0,subtotal;
    	ConAzucar	obj = new ConAzucar();
    	
    	while(opc)
    	{
    		try
    		{
    			System.out.println("\n\t1- Comprar Limonada \n\t2- Ventas de hoy \n\t3- Salir");
    			System.out.print("\tElija una opci�n: ");
    			op=Integer.parseInt(br.readLine());
    			switch(op)
    			{	
    				case 1:
    					anotherone='s';
    					obj = new ConAzucar();
    					do{
    						try{	
    						do{
    							System.out.println("\n\t1- Con Azucar \n\t2- Sin Azucar");
    							System.out.print("\tElija una opci�n: ");
    							op=Integer.parseInt(br.readLine());
    						}while(op!=1 && op!=2);	//Solo sale si agarra 1 o 2
							//CARGANDO DATOS
    						System.out.println("\n\t�Cuantos Vasos llevar�?");
    						vasos=Integer.parseInt(br.readLine());
    						
    						if(obj.mostrar_vasos()<vasos)
    							{
    								do{
    								System.out.println("\n\tUppss disculpe pero solo disponemos de: "+obj.mostrar_vasos()+" vasos.");
    								System.out.println("\n\t�Desea llevarse lo que queda? si/no");
    								anotherone=(br.readLine()).charAt(0);
    								}while(anotherone!='s' && anotherone!='n');//Solo sale si agarra s o n
    								
    								if(anotherone=='s')
    								vasos=obj.mostrar_vasos();
    							}
    							
    						if(anotherone=='s')
    							{	if(op==1)	//CON AZUCAR
    								{
		    							do{
    										System.out.print("\n\tLa cantidad de cucharadas de az�car disponibles es de 1 a 3.");
    										System.out.print("\n\tCuantas desea echarle a las limonadas: ");
    										cuchara=Integer.parseInt(br.readLine());
    										if(cuchara<1 || cuchara>3)
    											System.out.print("\n\tDisculpe, solo son 3 cucharadas por vaso m�ximo");
    										if(obj.azu_rest()<(cuchara*vasos))
    										{	
    										  do{
    										  	if(obj.azu_rest()==0)
    										  		System.out.print("\n\tLo sentimos pero ya no tenemos azucar");
    										  			else
    										  			{
			    											System.out.print("\n\tDisculpe, Solo disponemos de "+obj.azu_rest()+" de azucar.");
    														System.out.println("\n\t�Desea suministrar de forma equitativa el azucar que disponemos? si/no");
    														anotherone=(br.readLine()).charAt(0);
    										  			}
    											if(anotherone!='s' && anotherone!='n')
    												System.out.print("\n\tDebes ingresar si o no.");

    											if(anotherone=='s'&& obj.azu_rest()!=0)	//si dice que si, procederemos a calcular un valor aproximado para distribuir el azucar en cada vaso
    											{
    												cuchara=obj.azu_rest()/vasos;
    											}
    										  }while(anotherone!='s' && anotherone!='n');
    										}
    									}while(cuchara<1 || cuchara>3);	//Tendra un limite de 1 a 3(forzar la ruptura del bucle)
    								obj.carga(cuchara,vasos);
    								subtotal=obj.C_Azucar();
    								}
    								else //SIN AZUCAR
    									subtotal=obj.calcularPrecio(vasos);
    									
    							total+=subtotal;
    							}
    						do
    						{	System.out.println("\n\t�Alguien mas? s/n");
    							anotherone=(br.readLine()).charAt(0);
    						}while(anotherone!='s' && anotherone!='n');//forzar la ruptuta del bucle
    						reply = anotherone=='s'? true: false; //OPERADOR TERNARIO es lo mismo que if/else
    						
    					}
    					catch (IOException io)
    					{
    					System.out.println(io);
    					reply=true;
	    				}
    					catch (NumberFormatException nfe)
    					{
    						System.out.println(nfe);
    						reply=true;
    					}
    				}while(reply);
    						break;
    				
    				case 2:
    						System.out.println("=================================================");
    						System.out.println("\t\t\t\tRESULTADOS DEL DIA");
    						System.out.print("=================================================");
    						System.out.print("\n\tHas conseguido: B/."+total);
    						System.out.print("\n\tLimonadas Vendidas: "+obj.total_vasos_utilizados());
    						System.out.println("\n\tAzucar consumida: "+obj.total_azucar());
    						System.out.println("--------------------------------------------------");
    						System.out.print("\n\n\tPulse ENTER para regresar al men� o 3 para salir: ");
							br.readLine();
    						break;
    				
    				case 3: System.out.println("Hasta luego que tenga un lindo d�a");
    						opc=false;
    						break;
    						
    				default: System.out.println("Vas riport prro");
    			}
    		}
    		catch (IOException io)
    			{
    				System.err.println("\n\tError en la lectura. Vuelva a intentarlo");
    				reply=true;
	    		}
    		catch (NumberFormatException nfe)
    			{
    				System.err.println("\n\tRecuerde que solo se pueden ingresar n�meros enteros");
    				reply=true;
    			}
    		catch(Exception e)
    		{
    			System.err.println("\n\tHa ocurrido un error de tipo: "+e+" por favor notifiquelo.");
    		}
    	}
    }
}